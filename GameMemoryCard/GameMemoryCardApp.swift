//
//  GameMemoryCardApp.swift
//  GameMemoryCard
//
//  Created by informatics on 2/19/21.
//

import SwiftUI

@main
struct GameMemoryCardApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
