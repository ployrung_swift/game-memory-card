//
//  ContentView.swift
//  GameMemoryCard
//
//  Created by informatics on 2/19/21.
//

import SwiftUI

struct ContentView: View {
    @State var check = [false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false]
    
    var images = ["1","2","3","4","5","6","7","8","1","2","3","4","5","6","7","8"]
    var num = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
    var shuf: [Int]
    
    
    init(){
        self.shuf = num.shuffled()
        print(self.shuf)
    }
    @State var array: [Int] = []
    @State var pos: [Int] = []
    @State var clear = false
    func checkImg(imgIndex : Int,position : Int){
        //        print(images[imgIndex])
        self.check[position] = true
        if(clear == true){
            self.check[pos[0]] = false
            self.check[pos[1]] = false
            clear = false
            array.removeAll()
            pos.removeAll()
        }
        array.append(imgIndex)
        pos.append(position)
        if(array.count == 2){
            print(images[array[0]]," VS ",images[array[1]])
            if(images[array[0]] != images[array[1]]){
                print("No")
                clear = true
            }else{
                print("Same")
                array.removeAll()
                pos.removeAll()
                
            }
        }
    }
    
    var body: some View {
        VStack{
            HStack{
                let i1 = self.shuf[0]
                Button(action: {checkImg(imgIndex: i1,position: 0)}) {
                    if(check[0] == true){
                        Image(images[i1]).resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }else{
                        Image("back")
                            .resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }
                }
                let i2 = self.shuf[1]
                Button(action: {checkImg(imgIndex: i2,position: 1)}) {
                    if(check[1] == true){
                        Image(images[i2]).resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }else{
                        Image("back")
                            .resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }
                }
                let i3 = self.shuf[2]
                Button(action: {checkImg(imgIndex: i3,position: 2)}) {
                    if(check[2] == true){
                        Image(images[i3]).resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }else{
                        Image("back")
                            .resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }
                }
                let i4 = self.shuf[3]
                Button(action: {checkImg(imgIndex: i4,position: 3)}) {
                    if(check[3] == true){
                        Image(images[i4]).resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }else{
                        Image("back")
                            .resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }
                }
            }
            
            HStack{
                let i5 = self.shuf[4]
                Button(action: {checkImg(imgIndex: i5,position: 4)}) {
                    if(check[4] == true){
                        Image(images[i5]).resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }else{
                        Image("back")
                            .resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }
                }
                let i6 = self.shuf[5]
                Button(action: {checkImg(imgIndex: i6,position: 5)}) {
                    if(check[5] == true){
                        Image(images[i6]).resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }else{
                        Image("back")
                            .resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }
                }
                let i7 = self.shuf[6]
                Button(action: {checkImg(imgIndex: i7,position: 6)}) {
                    if(check[6] == true){
                        Image(images[i7]).resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }else{
                        Image("back")
                            .resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }
                }
                let i8 = self.shuf[7]
                Button(action: {checkImg(imgIndex: i8,position: 7)}) {
                    if(check[7] == true){
                        Image(images[i8]).resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }else{
                        Image("back")
                            .resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }
                }
            }
            HStack{
                let i9 = self.shuf[8]
                Button(action: {checkImg(imgIndex: i9,position: 8)}) {
                    if(check[8] == true){
                        Image(images[i9]).resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }else{
                        Image("back")
                            .resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }
                }
                let i10 = self.shuf[9]
                Button(action: {checkImg(imgIndex: i10,position: 9)}) {
                    if(check[9] == true){
                        Image(images[i10]).resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }else{
                        Image("back")
                            .resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }
                }
                let i11 = self.shuf[10]
                Button(action: {checkImg(imgIndex: i11,position: 10)}) {
                    if(check[10] == true){
                        Image(images[i11]).resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }else{
                        Image("back")
                            .resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }
                }
                let i12 = self.shuf[11]
                Button(action: {checkImg(imgIndex: i12,position: 11)}) {
                    if(check[11] == true){
                        Image(images[i12]).resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }else{
                        Image("back")
                            .resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }
                }            }
            HStack{
                let i13 = self.shuf[12]
                Button(action: {checkImg(imgIndex: i13,position: 12)}) {
                    if(check[12] == true){
                        Image(images[i13]).resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }else{
                        Image("back")
                            .resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }
                }
                let i14 = self.shuf[13]
                Button(action: {checkImg(imgIndex: i14,position: 13)}) {
                    if(check[13] == true){
                        Image(images[i14]).resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }else{
                        Image("back")
                            .resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }
                }
                
                let i15 = self.shuf[14]
                Button(action: {checkImg(imgIndex: i15,position: 14)}) {
                    if(check[14] == true){
                        Image(images[i15]).resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }else{
                        Image("back")
                            .resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }
                }
                let i16 = self.shuf[15]
                Button(action: {checkImg(imgIndex: i16,position: 15)}) {
                    if(check[15] == true){
                        Image(images[i16]).resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }else{
                        Image("back")
                            .resizable()
                            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .padding(5)
                            
                            .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                    }
                }
            }
        }
    }
    
    struct ContentView_Previews: PreviewProvider {
        static var previews: some View {
            ContentView()
        }
    }
}
